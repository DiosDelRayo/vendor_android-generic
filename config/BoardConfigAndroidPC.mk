# Android-PC optimizations
WITH_BLISS_CHARGER := false
WITH_LINEAGE_CHARGER := false
WITH_TESLA_CHARGER := false
WITH_TIPSY_CHARGER := false
TARGET_PC_BUILD := true
# USE_HOUDINI := true
# TARGET_USE_AOSP_SURFACEFLINGER := true

# Android PC specific 
RELEASE_OS_TITLE := Android-PC
FLOOP_NEST_OPTIMIZE := true
FAST_MATH := true
LINK_TIME_OPTIMIZATIONS := true

# Used for some ROMs instead of build/make/0006-All-operating-systems-are-beautiful.patch
TARGET_SKIP_OTATOOLS_PACKAGE := true

# Android-PC overrides
#BOARD_KERNEL_IMAGE_NAME := kernel
#LINEAGE_BUILD := true
#BUILD_KERNEL_WITH_CLANG :=true

-include vendor/bliss/config/BoardConfigBliss.mk
-include vendor/tesla/config/BoardConfigTESLA.mk
-include vendor/tipsy/config/BoardConfigTipsy.mk

