#!/bin/bash

rom_fp="$(date +%y%m%d)"
rompath=$(pwd)
vendor_path="android-generic"
mkdir -p release/$rom_fp/
# Comment out 'set -e' if your session terminates.
set -e

#setup colors
red=`tput setaf 1`
green=`tput setaf 2`
yellow=`tput setaf 3`
blue=`tput setaf 4`
purple=`tput setaf 5`
teal=`tput setaf 6`
light=`tput setaf 7`
dark=`tput setaf 8`
CL_CYN=`tput setaf 12`
CL_RST=`tput sgr0`
reset=`tput sgr0`

localManifestBranch="q"
rom=""
product=""
ag_variant=""
ag_variant_name=""
ag_release="n"
bliss_partiton=""
filename=""
file_size=""
clean="n"
sync="n"
patch="n"
romBranch=""
backup="n"
ver=$(date +"%F")
vendorsetup="n"

ask() {
    # https://djm.me/ask
    local prompt default reply

    if [ "${2:-}" = "Y" ]; then
        prompt="Y/n"
        default=Y
    elif [ "${2:-}" = "N" ]; then
        prompt="y/N"
        default=N
    else
        prompt="y/n"
        default=
    fi

    while true; do

        # Ask the question (not using "read -p" as it uses stderr not stdout)
        echo -n "$1 [$prompt] "

        # Read the answer (use /dev/tty in case stdin is redirected from somewhere else)
        read reply </dev/tty

        # Default?
        if [ -z "$reply" ]; then
            reply=$default
        fi

        # Check if the reply is valid
        case "$reply" in
            Y*|y*) return 0 ;;
            N*|n*) return 1 ;;
        esac

    done
}

if [ -z "$USER" ];then
        export USER="$(id -un)"
fi
export LC_ALL=C

if [[ $(uname -s) = "Darwin" ]];then
        jobs=$(sysctl -n hw.ncpu)
elif [[ $(uname -s) = "Linux" ]];then
        jobs=$(nproc)
fi


while test $# -gt 0
do
  case $1 in

  # Normal option processing
    -h | --help)
      echo "Usage: $0 options buildVariants"
      echo "options: -c | --clean : Does make clean && make clobber and resets the treble device tree"
      echo "         -r | --release : Builds a twrp zip of the rom (only for A partition) default creates system.img"
      echo "         -s | --sync: Repo syncs the rom (clears out patches), then reapplies patches to needed repos"
      echo "		 -p | --patch: Run the patches only"
      echo "		 -b | --backup: Generate a manifest backup with revisions included. (Helpful when ROMs update miltiple times a week)"
      echo "		 -n | --vendor-setup __vendorname__ : Creates all the required folders & files to start building. "
      echo ""
      echo "buildVariants: arm64_a_stock | arm64_ab_stock | arm_a_stock | arm_ab_stock | a64_a_stock | a64_ab_stock: Vanilla Rom"
      echo "                arm64_a_gapps | arm64_ab_gapps | arm_a_gapps | arm_ab_gapps | a64_a_gapps | a64_ab_gapps : Stock Rom with Gapps Built-in"
      echo "                arm64_a_foss | arm64_ab_foss | arm_a_foss | arm_ab_foss | a64_a_foss | a64_ab_foss : Stock Rom with Foss"
      echo "                arm64_a_go | arm64_ab_go | arm_a_go | arm_ab_go | a64_a_go | a64_ab_go : Stock Rom with Go-Gapps"
      echo ""
      echo "blissBranch: select which bliss branch to sync, default is o8.1-los"
      ;;
    -v | --version)
      echo "Version: Bliss Treble Builder 0.3"
      echo "Updated: 7/29/2018"
      ;;
    -c | --clean)
      clean="y";
      echo "Cleaning build and device tree selected."
      ;;
    -r | --release)
      ag_release="y";
      echo "Building as release selected."
      ;;
    -s | --sync)
      sync="y"
      echo "Repo syncing and patching selected."
      ;;
    -p | --patch)
      patch="y";
      echo "patching selected."
      ;;
	-b | --backup)
	  backup="y";
	  echo "Generating a backup manifest with revisions..."
	  ;;
	-n | --vendor-setup)
	  vendorsetup="y";
	  echo "Setting up $1 vendor..."
	  ;;
  # ...

  # Special cases
    --)
      break
      ;;
    --*)
      # error unknown (long) option $1
      ;;
    -?)
      # error unknown (short) option $1
      ;;

  # FUN STUFF HERE:
  # Split apart combined short options
    -*)
      split=$1
      shift
      set -- $(echo "$split" | cut -c 2- | sed 's/./-& /g') "$@"
      continue
      ;;

  # Done with options
    *)
      break
      ;;
  esac

  # for testing purposes:
  shift
done

if [ "$1" = "arm64_a_stock" ];then
        ag_variant=treble_arm64_avN-userdebug;
        ag_variant_name=arm64-a-stock;
        ag_partition="a";
        ag_variant_type=phhgsi_arm64_a

elif [ "$1" = "arm64_a_gapps" ];then
        ag_variant=treble_arm64_agS-userdebug;
        ag_variant_name=arm64-a-gapps;
        ag_partition="a";
        ag_variant_type=phhgsi_arm64_a

elif [ "$1" = "arm64_a_foss" ];then
        ag_variant=treble_arm64_afS-userdebug;
        ag_variant_name=arm64-a-foss;
        ag_partition="a";
        ag_variant_type=phhgsi_arm64_a

elif [ "$1" = "arm64_a_go" ];then
        ag_variant=treble_arm64_aoS-userdebug;
        ag_variant_name=arm64-a-go;
        ag_partition="a";
        ag_variant_type=phhgsi_arm64_a

elif [ "$1" = "arm64_ab_stock" ];then
        ag_variant=treble_arm64_bvN-userdebug;
        ag_variant_name=arm64-ab-stock;
        ag_partition="ab";
        ag_variant_type=phhgsi_arm64_ab

elif [ "$1" = "arm64_ab_gapps" ];then
        ag_variant=treble_arm64_bgS-userdebug;
        ag_variant_name=arm64-ab-gapps;
        ag_partition="ab";
        ag_variant_type=phhgsi_arm64_ab

elif [ "$1" = "arm64_ab_foss" ];then
        ag_variant=treble_arm64_bfS-userdebug;
        ag_variant_name=arm64-ab-foss;
        ag_partition="ab";
        ag_variant_type=phhgsi_arm64_ab

elif [ "$1" = "arm64_ab_go" ];then
        ag_variant=treble_arm64_boS-userdebug;
        ag_variant_name=arm64-ab-go;
        ag_partition="ab";
        ag_variant_type=phhgsi_arm64_ab
        
        
elif [ "$1" = "a64_a_stock" ];then
        ag_variant=treble_a64_avN-userdebug;
        ag_variant_name=a64-a-stock;
        ag_partition="a";
        ag_variant_type=phhgsi_a64_a

elif [ "$1" = "a64_a_gapps" ];then
        ag_variant=treble_a64_agS-userdebug;
        ag_variant_name=a64-a-gapps;
        ag_partition="a";
        ag_variant_type=phhgsi_a64_a

elif [ "$1" = "a64_a_foss" ];then
        ag_variant=treble_a64_afS-userdebug;
        ag_variant_name=a64-a-foss;
        ag_partition="a";
        ag_variant_type=phhgsi_a64_a

elif [ "$1" = "a64_a_go" ];then
        ag_variant=treble_a64_aoS-userdebug;
        ag_variant_name=a64-a-go;
        ag_partition="a";
        ag_variant_type=phhgsi_a64_a

elif [ "$1" = "a64_ab_stock" ];then
        ag_variant=treble_a64_bvN-userdebug;
        ag_variant_name=a64-ab-stock;
        ag_partition="ab";
        ag_variant_type=phhgsi_a64_ab

elif [ "$1" = "a64_ab_gapps" ];then
        ag_variant=treble_a64_bgS-userdebug;
        ag_variant_name=a64-ab-gapps;
        ag_partition="ab";
        ag_variant_type=phhgsi_a64_ab

elif [ "$1" = "a64_ab_foss" ];then
        ag_variant=treble_a64_bfS-userdebug;
        ag_variant_name=a64-ab-foss;
        ag_partition="ab";
        ag_variant_type=phhgsi_a64_ab

elif [ "$1" = "a64_ab_go" ];then
        ag_variant=treble_a64_boS-userdebug;
        ag_variant_name=a64-ab-go;
        ag_partition="ab";
        ag_variant_type=phhgsi_a64_ab
        


elif [ "$1" = "arm_a_stock" ];then
        ag_variant=treble_arm_avN-userdebug;
        ag_variant_name=arm-a-stock;
        ag_partition="a";
        ag_variant_type=phhgsi_arm_a

elif [ "$1" = "arm_a_gapps" ];then
        ag_variant=treble_arm_agS-userdebug;
        ag_variant_name=arm-a-gapps;
        ag_partition="a";
        ag_variant_type=phhgsi_arm_a

elif [ "$1" = "arm_a_foss" ];then
        ag_variant=treble_arm_afS-userdebug;
        ag_variant_name=arm-a-foss;
        ag_partition="a";
        ag_variant_type=phhgsi_arm_a

elif [ "$1" = "arm_a_go" ];then
        ag_variant=treble_arm_aoS-userdebug;
        ag_variant_name=arm-a-go;
        ag_partition="a";
        ag_variant_type=phhgsi_arm_a

elif [ "$1" = "arm_ab_stock" ];then
        ag_variant=treble_arm_bvN-userdebug;
        ag_variant_name=arm-ab-stock;
        ag_partition="ab";
        ag_variant_type=phhgsi_arm_ab

elif [ "$1" = "arm_ab_gapps" ];then
        ag_variant=treble_arm_bgS-userdebug;
        ag_variant_name=arm-ab-gapps;
        ag_partition="ab";
        ag_variant_type=phhgsi_arm_ab

elif [ "$1" = "arm_ab_foss" ];then
        ag_variant=treble_arm_bfS-userdebug;
        ag_variant_name=arm-ab-foss;
        ag_partition="ab";
        ag_variant_type=phhgsi_arm_ab

elif [ "$1" = "arm_ab_go" ];then
        ag_variant=treble_arm_boS-userdebug;
        ag_variant_name=arm-ab-go;
        ag_partition="ab";
        ag_variant_type=phhgsi_arm_ab        
        
else
        echo "you need to at least use '--help'"
fi

#~ if [ "$2" = "" ];then
   #~ romBranch="q"
   #~ echo "Using branch $romBranch for repo syncing Bliss."
#~ else
   #~ romBranch="$2"
   #~ echo "Using branch $romBranch for repo syning Bliss."
#~ fi


if [ $sync == "y" ];then
	rm -rf $rompath/.repo/local_manifests

	mkdir -p $rompath/.repo/local_manifests
	echo -e ${CL_CYN}""${CL_RST}

	echo "Searching for vendor..."
	while IFS= read -r agvendor_name; do
		if [ -d $rompath/vendor/$agvendor_name/ ]; then
		  export ROM_IS_$agvendor_name=true
		  romname=$agvendor_name
		  echo "Found $agvendor!!"
		  echo -e ${CL_CYN}""${CL_RST}
		  echo -e ${CL_CYN}"copying $agvendor_name specific manifest files..."${CL_RST}
		  cp -r $rompath/vendor/$vendor_path/manifests/treble/$agvendor_name/* $rompath/.repo/local_manifests
		else
		  echo ""
		fi
	done < $rompath/vendor/$vendor_path/gsi_roms.lst
	
	echo -e ${CL_CYN}""${CL_RST}
	
	cp -r $rompath/vendor/$vendor_path/manifests/treble/local_manifests/* $rompath/.repo/local_manifests

	if [ -d $rompath/device/generic/common/ ]; then
		# do nothing
		echo "device/generic/common found"
	else
		echo "device/generic/common not found"
		echo ""
		echo "Getting that now"
		mkdir device/generic
		cd device/generic
		git clone --single-branch https://android.googlesource.com/device/generic/common
		cd -
	fi
	
	# Search for manifest backups
	while IFS= read -r agvendor_name; do
		echo -e ${CL_CYN}""${CL_RST}
		
		if [ -d $rompath/vendor/$agvendor_name/ ]; then
			echo "Found a vendor manifest backup: $agvendor_name"
			export ROM_IS_$agvendor_name=true
			romname=$agvendor_name
			if [ -f $rompath/.repo/manifests/$romname-manifest.xml ]; then 
				echo "You are already using the $romname manifest backup"
				rm -rf $rompath/.repo/local_manifests
			else 
				if [ -d $rompath/vendor/android-generic/vendor-manifests/gsi-vendor-manifests/$romname/ ]; then
					echo -e ${CL_CYN}""${CL_RST}
					echo "Found a $romname manifest backup"
					
					if ask "Do you want to use the backup manifest for "$agvendor_name"? \nIf not, you will likely face patch conflicts from updates to the ROM"; then
						echo "You chose to use our $agvendor_name manifest backup"
						rm -rf $rompath/.repo/local_manifests
						echo "remove local_manifests"	
						echo -e ${CL_CYN}""${CL_RST}
						echo "Copying manifest to $rompath/.repo/manifests/$romname-manifest.xml"
						cp $rompath/vendor/android-generic/vendor-manifests/gsi-vendor-manifests/$romname/$romname-manifest.xml $rompath/.repo/manifests/$romname-manifest.xml
						
						cd $rompath/.repo/manifests
						git add -A && git commit -am "Temp Manifest - Revert If needed" --author="Jon West <electrikjesus@gmail.com>"
						cd $rompath
						echo "Running init for the $romname backup manifest"
						repo init -m $romname-manifest.xml
					else
						echo "You chose not to use the $romname backup"
					fi
					
				else
					echo -e ${CL_CYN}""${CL_RST}
				fi
			fi
		else
		  echo -e ${CL_CYN}""${CL_RST}
		fi
	done < $rompath/vendor/$vendor_path/gsi_roms.lst

	repo sync -c -j$jobs --no-tags --no-clone-bundle --force-sync

else
        echo "Not gonna sync this round"
fi

# rm -f device/*/sepolicy/common/private/genfs_contexts

if [ $clean == "y" ];then
	echo "generating phh $rom product files"
    (cd device/phh/treble; git clean -fdx; bash generate.sh $rom)
    make clean && make clobber
else
	echo "generating phh $rom product files"
    (cd device/phh/treble; bash generate.sh $rom)
fi
. build/envsetup.sh

# sed -i -e 's/BOARD_SYSTEMIMAGE_PARTITION_SIZE := 1610612736/BOARD_SYSTEMIMAGE_PARTITION_SIZE := 2147483648/g' device/phh/treble/phhgsi_arm64_a/BoardConfig.mk

#if  [ $sync == "y" ];then
#if [ -z "$local_patches" ];then
#    if [ -d patches ];then
#        ( cd vendor/bliss/treble/patches/patches; git fetch; git reset --hard; git checkout origin/$localManifestBranch)
#    else
#        git clone https://github.com/aclegg2011/treble_patches vendor/bliss/treble/patches -b $localManifestBranch
#    fi
#else
#    rm -Rf vendor/bliss/treble/patches/patches
#    mkdir vendor/bliss/treble/patches/patches
#    unzip  "$local_patches" -d patches
#fi
#echo "Let the patching begin"
#bash "$rompath/vendor/bliss/treble/apply-patches.sh" $rompath/vendor/bliss/treble/patches/patches
#fi

#blissRelease(){
#if [[ "$1" = "y" && $ag_partition = "a" ]];then
#       echo "Building twrp flashable gsi.."
#        if [ -d $OUT/img2sdat ] ;then
#             cp -r $rompath/build/make/core/treble/img2sdat/* $OUT/img2sdat
#         else
#             mkdir -p $OUT/img2sdat
#             cp -r $rompath/build/make/core/treble/img2sdat/* $OUT/img2sdat
#        fi
#        if [ -d $OUT/twrp_flashables ] ;then
#             cp -r $rompath/build/make/core/treble/twrp_flashables/* $OUT/twrp_flashables
#         else
#             mkdir -p $OUT/twrp_flashables
#             cp -r $rompath/build/make/core/treble/twrp_flashables/* $OUT/twrp_flashables
#        fi
#        cp $OUT/system.img $OUT/img2sdat/system.img
#        cd $OUT/img2sdat
#        ./img2sdat.py system.img -o tmp -v 4
#        cd $rompath
#        cp $OUT/img2sdat/tmp/* $OUT/twrp_flashables/arm64a
#        cd $OUT/twrp_flashables/arm64a
#        7za a -tzip arm64a.zip *
#        cd $rompath
#        cp $OUT/twrp_flashables/arm64a/arm64a.zip $rompath/release/$rom_fp/Bliss-$ver-$ag_variant_name.zip
#        rm -rf $OUT/img2sdat $OUT/twrp_flashables
#        filename=Bliss-$ver-$ag_variant_name.zip
#        filesize=$(stat -c%s release/$rom_fp/$filename)
#        md5sum release/$rom_fp/Bliss-$ver-$ag_variant_name.zip > release/$rom_fp/Bliss-$ver-$ag_variant_name.zip.md5
#        md5sum_file=Bliss-$ver-$ag_variant_name.zip.md5
#else
#        if  [[ "$1" = "n" || $ag_partition = "ab" ]];then
#        echo "Twrp Building is currently not suported on a/b"
#        fi
#        echo "Compressing and Copying $OUT/system.img -> release/$rom_fp/Bliss-$ver-$ag_variant_name.img.xz"
#        xz -c $OUT/system.img > release/$rom_fp/Bliss-$ver-$ag_variant_name.img.xz
#        filename=Bliss-$ver-$ag_variant_name.img.xz
#        filesize=$(stat -c%s release/$rom_fp/$filename)
#        md5sum release/$rom_fp/Bliss-$ver-$ag_variant_name.img.xz  > release/$rom_fp/Bliss-$ver-$ag_variant_name.img.xz.md5
#        md5sum_file=Bliss-$ver-$ag_variant_name.img.xz.md5
#fi
#}

agRelease(){
	
	if [ -f $rompath/vendor/$vendor_path/patches/google_diff/treble_vendor_patches/${CURRENT_ROM}/config.lst ]; then
		while IFS= read -r agrom_name; do
			echo -e ${CL_CYN}""${CL_RST}
			echo "ROM name being used is: $agrom_name"
			ROM_VENDOR_CUSTOM_NAME=$agrom_name
			
		done < $rompath/vendor/$vendor_path/patches/google_diff/treble_vendor_patches/${CURRENT_ROM}/config.lst
	else 
		echo " $rompath/vendor/$vendor_path/patches/google_diff/treble_vendor_patches/${CURRENT_ROM}/config.lst not found"
		echo -e ${CL_CYN}""${CL_RST}
		touch $rompath/vendor/$vendor_path/patches/google_diff/treble_vendor_patches/${CURRENT_ROM}/config.lst
		read -p "What do you call this ROM? (what is the ROM's name): " agrom_name
		echo "$agrom_name" >> $rompath/vendor/$vendor_path/patches/google_diff/treble_vendor_patches/${CURRENT_ROM}/config.lst
		echo "ROM name is setup"
		ROM_VENDOR_CUSTOM_NAME=$agrom_name
	fi
	
	if  [ "$product_name" != "Android-Generic" ];then
	filename=$product_name$ver-$ag_variant_name ;
	else 
	filename=Android-Generic_$product_name$ver-$ag_variant_name ;
	fi
	if [ "$ROM_VENDOR_CUSTOM_NAME" != "" ];then
	echo "Filename being used is: ${ROM_VENDOR_CUSTOM_NAME}-$ver-$ag_variant_name"
	filename=${ROM_VENDOR_CUSTOM_NAME}-$ver-$ag_variant_name ;
	fi
	
	echo "moving file"
	mv $OUT/system.img $OUT/$filename.img
	echo "making md5sum"
	md5sum $OUT/$filename.img  > $OUT/$filename.md5
	md5sum_file=$OUT/$filename.md5	
	filesize=$(stat -c%s $OUT/$filename.img)
}


if  [ $patch == "y" ] || [ $sync == "y" ];then
	echo "Let the patching begin"
	bash "$rompath/vendor/$vendor_path/prepatch_vendor_treble.sh"
	bash "$rompath/vendor/$vendor_path/autopatch_treble.sh"
	bash "$rompath/vendor/$vendor_path/autopatch_vendor_treble.sh"
	bash "$rompath/vendor/$vendor_path/custompatch_vendor_treble.sh"
fi

if [ $backup == "y" ];then
	echo "Backup starting..."
	
	while IFS= read -r agvendor_name; do
		echo -e ${CL_CYN}""${CL_RST}
		echo "Searching for vendor: $agvendor_name"
		if [ -d $rompath/vendor/$agvendor_name/ ]; then
			export ROM_IS_$agvendor_name=true
			romname=$agvendor_name
			echo -e ${CL_CYN}""${CL_RST}
			echo "making a $romname manifest backup now"			
			echo "This might take a while..."
			repo manifest -o agmanifest.xml -r
			echo -e ${CL_CYN}""${CL_RST}
			echo "manifest created"
			if [ -d $rompath/vendor/android-generic/vendor-manifests/gsi-vendor-manifests/$romname/ ]; then
				mv agmanifest.xml vendor/android-generic/vendor-manifests/gsi-vendor-manifests/$romname/$romname-manifest.xml
				echo -e ${CL_CYN}""${CL_RST}
				echo "Moving manifest to vendor/android-generic/vendor-manifests/gsi-vendor-manifests/$romname/$romname-manifest.xml"
			else
				mkdir -p vendor/android-generic/vendor-manifests/gsi-vendor-manifests/$romname
				mv agmanifest.xml vendor/android-generic/vendor-manifests/gsi-vendor-manifests/$romname/$romname-manifest.xml
				echo -e ${CL_CYN}""${CL_RST}
				echo "Moving manifest to vendor/android-generic/vendor-manifests/gsi-vendor-manifests/$romname/$romname-manifest.xml"
			fi
		else
		  echo "Not Found"
		fi
	done < $rompath/vendor/$vendor_path/gsi_roms.lst
	
fi

if [ $vendorsetup == "y" ]; then
	echo "Setting up $1 vendor files"
	if ask "Do you want to create the base files/folders/etc for "$1"?"; then
		echo "You chose to setup $1 for GSI builds"
		
		# prepatches
		if [ -d $rompath/vendor/$vendor_path/patches/google_diff/treble_vendor_prepatches/$1 ]; then
			echo "It looks to be already setup. If this is a mistake, please remove the folder"
			echo "$rompath/vendor/$vendor_path/patches/google_diff/treble_vendor_prepatches/$1 and try again"
		else
			mkdir $rompath/vendor/$vendor_path/patches/google_diff/treble_vendor_prepatches/$1
		fi
		
		# patches
		if [ -d $rompath/vendor/$vendor_path/patches/google_diff/treble_vendor_patches/$1 ]; then
			echo "It looks to be already setup. If this is a mistake, please remove the folder"
			echo "$rompath/vendor/$vendor_path/patches/google_diff/treble_vendor_patches/$1 and try again"
		else
			mkdir $rompath/vendor/$vendor_path/patches/google_diff/treble_vendor_patches/$1
			mkdir $rompath/vendor/$vendor_path/patches/google_diff/treble_vendor_patches/$1/patches
			mkdir $rompath/vendor/$vendor_path/patches/google_diff/treble_vendor_patches/$1/custom_patches
			touch $rompath/vendor/$vendor_path/patches/google_diff/treble_vendor_patches/$1/custom_patches/customizations.lst
			mkdir $rompath/vendor/$vendor_path/patches/google_diff/treble_vendor_patches/$1/overlay
			echo "$1" >> $rompath/vendor/$vendor_path/gsi_roms.lst
			echo '$(call inherit-product-if-exists, vendor/'$1'/config/common.mk)' >> $rompath/vendor/$vendor_path/config/vendors.mk
			echo "All patch folders are setup"
		fi
		
		# manifests
		if [ -d $rompath/vendor/$vendor_path/manifests/treble/$1 ]; then
			echo "There looks to already be a manifest folder setup for this. Please remove this folder if it was created by mistake"
			echo "$rompath/vendor/$vendor_path/manifests/treble/$1"
		else
			mkdir $rompath/vendor/$vendor_path/manifests/treble/$1
			cp $rompath/vendor/$vendor_path/manifests/treble/stock/* $rompath/vendor/$vendor_path/manifests/treble/$1
			echo "All manifest folders are setup"
		fi
		
		# ROM Name
		if [ -f $rompath/vendor/$vendor_path/patches/google_diff/treble_vendor_patches/$1/config.lst ]; then
			echo "config.lst looks to be already setup. If this is a mistake, please remove the file"
			echo "$rompath/vendor/$vendor_path/patches/google_diff/treble_vendor_patches/$1/config.lst and try again"
		else
			touch $rompath/vendor/$vendor_path/patches/google_diff/treble_vendor_patches/$1/config.lst
			read -p "What do you call this ROM? (what is the ROM's name): " vsromname
			echo "$VSROMNAME" >> $rompath/vendor/$vendor_path/patches/google_diff/treble_vendor_patches/$1/config.lst
			
			echo "ROM name is setup"
		fi
		
	else
		echo "You chose not to setup $1 for GSI builds"
	fi
fi

while IFS= read -r agvendor_name; do
	#~ echo -e ${CL_CYN}""${CL_RST}
	#~ echo "Searching for vendor: $agvendor_name"
	if [ -d $rompath/vendor/$agvendor_name/ ]; then
		echo "Found $agvendor_name, setting that as CURRENT_ROM"
		export CURRENT_ROM=$agvendor_name		
	#~ else
	  #~ echo "Not Found"
	fi
done < $rompath/vendor/$vendor_path/gsi_roms.lst
echo "CURRENT_ROM=${CURRENT_ROM}"

# Begin builds

blissHeader(){
        file_size=$(echo "${filesize}" | awk '{ split( "B KB MB GB TB PB" , v ); s=1; while( $1>1024 ){ $1/=1024; s++ } printf "%.2f %s", $1, v[s] }')
        echo -e ""
        echo -e "=====-Android-Generic GSI Package Complete-====="
        echo -e "File: $1"
        echo -e "MD5: $3"
        echo -e "Size: $file_size"
        echo -e "==============================================="
        echo -e "Thank you for contributing "
        echo -e "==============================================="
        echo -e ""
}

if [[
"$1" = "arm64_a_stock" ||
"$1" = "arm64_a_gapps" ||
"$1" = "arm64_a_foss" ||
"$1" = "arm64_a_go" ||
"$1" = "arm64_ab_stock" ||
"$1" = "arm64_ab_gapps" ||
"$1" = "arm64_ab_foss" ||
"$1" = "arm64_ab_go" 
||
"$1" = "a64_a_stock" ||
"$1" = "a64_a_gapps" ||
"$1" = "a64_a_foss" ||
"$1" = "a64_a_go" ||
"$1" = "a64_ab_stock" ||
"$1" = "a64_ab_gapps" ||
"$1" = "a64_ab_foss" ||
"$1" = "a64_ab_go" 
||
"$1" = "arm_a_stock" ||
"$1" = "arm_a_gapps" ||
"$1" = "arm_a_foss" ||
"$1" = "arm_a_go" ||
"$1" = "arm_ab_stock" ||
"$1" = "arm_ab_gapps" ||
"$1" = "arm_ab_foss" ||
"$1" = "arm_ab_go" 
]];then
echo "Setting up build env for $1"
. build/envsetup.sh
fi

#~ buildVariant() {
                #~ ## echo "running lunch for $1"
        #~ ## lunch $1
        #~ echo "Running lunch for $ag_variant"
        #~ lunch $ag_variant
        #~ make WITHOUT_CHECK_API=true BUILD_NUMBER=$rom_fp installclean
        #~ make WITHOUT_CHECK_API=true BUILD_NUMBER=$rom_fp -j$jobs systemimage
        #~ make WITHOUT_CHECK_API=true BUILD_NUMBER=$rom_fp vndk-test-sepolicy
        #~ # blissRelease $ag_release $ag_partition
        #~ blissHeader $filename $filesize $md5sum_file
#~ }

buildVariant() {
                ## echo "running lunch for $1"
        ## lunch $1
        echo "Running lunch for $ag_variant"
        lunch $ag_variant
        make -j$jobs systemimage
        agRelease $ag_release $ag_partition
        blissHeader $filename $filesize $md5sum_file
}

if [[
"$1" = "arm64_a_stock" ||
"$1" = "arm64_a_gapps" ||
"$1" = "arm64_a_foss" ||
"$1" = "arm64_a_go" ||
"$1" = "arm64_ab_stock" ||
"$1" = "arm64_ab_gapps" ||
"$1" = "arm64_ab_foss" ||
"$1" = "arm64_ab_go" 
||
"$1" = "a64_a_stock" ||
"$1" = "a64_a_gapps" ||
"$1" = "a64_a_foss" ||
"$1" = "a64_a_go" ||
"$1" = "a64_ab_stock" ||
"$1" = "a64_ab_gapps" ||
"$1" = "a64_ab_foss" ||
"$1" = "a64_ab_go" 
||
"$1" = "arm_a_stock" ||
"$1" = "arm_a_gapps" ||
"$1" = "arm_a_foss" ||
"$1" = "arm_a_go" ||
"$1" = "arm_ab_stock" ||
"$1" = "arm_ab_gapps" ||
"$1" = "arm_ab_foss" ||
"$1" = "arm_ab_go" 
]];then
buildVariant $ag_variant $ag_variant_name
fi
